from django.shortcuts import render
from django.http import JsonResponse
import requests

# Create your views here.

def index(request):
    return render(request, 'home.html')

def web(request):
    return render(request, 'web.html')

def book_search(request):
    book_input = request.GET.get('keyword', None)
    if book_input == None:
        book_input='a'

    url = 'https://www.googleapis.com/books/v1/volumes?q='+ book_input
    result = requests.get(url).json()

    return JsonResponse(result)

