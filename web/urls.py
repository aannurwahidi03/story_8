from django.urls import path
from .views import index, web, book_search

urlpatterns = [
    path('', index, name='home'),
    path('web/', web, name='web'),
    path('web/book_search/', book_search, name="book_search")
]