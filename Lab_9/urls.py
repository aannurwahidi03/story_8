from django.urls import path
from .views import SignUp, LogOut

urlpatterns = [
    path('register/', SignUp.as_view(), name='register'),
    path('logout/', LogOut, name='logout')
]