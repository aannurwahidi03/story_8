from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
class Log_In(models.Model):
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=15)

class CustomUser(AbstractUser):
    pass
    