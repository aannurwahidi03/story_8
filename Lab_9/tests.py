from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.apps import apps

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

import time
import unittest
import os

# Create your tests here.
class ReportsConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(apps.get_app_config('Lab_9').name, 'Lab_9')

class UntitTest(TestCase):
    
    def test_if_url_is_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEquals(response.status_code, 200)

    def test_if_using_template_landing_page(self):
        response = Client().get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')

# class FunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome(executable_path='./chromedriver',chrome_options=chrome_options)
#         super(FunctionalTest, self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()

#     def test_if_user_login(self):
#         browser = self.browser
#         browser.get(self.live_server_url+"/")
